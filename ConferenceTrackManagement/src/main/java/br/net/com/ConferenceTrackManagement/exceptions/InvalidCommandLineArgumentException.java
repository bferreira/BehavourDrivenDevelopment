package br.net.com.ConferenceTrackManagement.exceptions;

public class InvalidCommandLineArgumentException extends IllegalArgumentException {
	public InvalidCommandLineArgumentException(String notFoundExceptionMessage) {
		super(notFoundExceptionMessage);
	}

	private static final long serialVersionUID = -4130048529514141948L;}
