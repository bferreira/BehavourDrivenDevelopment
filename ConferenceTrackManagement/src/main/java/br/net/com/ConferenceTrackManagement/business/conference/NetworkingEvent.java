package br.net.com.ConferenceTrackManagement.business.conference;

import java.time.LocalTime;

public class NetworkingEvent extends Activity {
	private static final String NETWORKING_EVENT = "Networking Event";

	public NetworkingEvent(boolean shouldBeEarlist) {
		super(NETWORKING_EVENT, shouldBeEarlist ? LocalTime.of(16, 00) : LocalTime.of(17, 00));
	}

}