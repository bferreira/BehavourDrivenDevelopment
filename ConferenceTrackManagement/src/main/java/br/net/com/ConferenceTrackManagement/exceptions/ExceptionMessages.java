package br.net.com.ConferenceTrackManagement.exceptions;

public class ExceptionMessages {
	private static final String WRITE_TYPES_ENTIRES = "writerType:console";
	private static final String FILE_TYPES_ENTRIES = "readerType:file";
	private static final String VALID_ENTRIES_ARE = ". Valid entries are: ";
	public static final String NO_INPUT_TYPE = "no input type defined" + VALID_ENTRIES_ARE + FILE_TYPES_ENTRIES;
	public static final String NO_WRITER_TYPE = "no writer type defined" + VALID_ENTRIES_ARE + WRITE_TYPES_ENTIRES;
	public static final String INVALID_INPUT_FILE_EXTENSION = "invalid input file extension, accepted extensions: .txt";
	public static final String INPUT_FILE_PATH_EMPTY = "Input file path empty, please inform an input file ex:";
	public static final String INVALID_INPUT_TYPE = "invalid input file" + VALID_ENTRIES_ARE + FILE_TYPES_ENTRIES;
}
