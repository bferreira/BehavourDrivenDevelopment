package br.net.com.ConferenceTrackManagement.business.conference;

import java.util.ArrayList;
import java.util.List;

public class ConferenceSchedule {
	private final List<Track> tracks;

	public ConferenceSchedule(List<Track> track) {
		this.tracks = track;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Track track : tracks) {
			sb.append(track);
		}
		return sb.toString();
	}

	public List<Activity> getAllDaysSchedule() {
		List<Activity> activities = new ArrayList<>();
		for (Track track : tracks) {
			activities.addAll(track.getAllActivities());
		}
		return activities;
	}


}
