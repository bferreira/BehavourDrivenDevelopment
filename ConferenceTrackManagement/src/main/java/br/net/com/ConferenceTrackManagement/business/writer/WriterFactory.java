package br.net.com.ConferenceTrackManagement.business.writer;

import br.net.com.ConferenceTrackManagement.business.commandline.OptionsParser;
import br.net.com.ConferenceTrackManagement.exceptions.ExceptionMessages;

public class WriterFactory {

	public Writer get(OptionsParser options) {
		switch (options.get("writerType",ExceptionMessages.NO_WRITER_TYPE)) {
		case "console":
			return new ConsoleWriter();
		default:
			return new ConsoleWriter();
		}
	}

}
