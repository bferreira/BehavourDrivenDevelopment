package br.net.com.ConferenceTrackManagement;

import java.util.List;

import br.net.com.ConferenceTrackManagement.business.conference.Activity;
import br.net.com.ConferenceTrackManagement.business.conference.ConferenceSchedule;
import br.net.com.ConferenceTrackManagement.business.reader.Reader;
import br.net.com.ConferenceTrackManagement.business.tracker.TrackAllocator;
import br.net.com.ConferenceTrackManagement.business.writer.Writer;

public class ConferenceTrackManager {
	private Reader reader;
	private TrackAllocator trackAllocator;
	private Writer writer;

	public ConferenceTrackManager(Reader reader, Writer writer) {
		this.reader = reader;
		this.writer = writer;
		this.trackAllocator = new TrackAllocator();
	}

	public ConferenceSchedule getSchedule() throws Exception {
		List<Activity> conferences = reader.read(); 
		ConferenceSchedule tracks = trackAllocator.getTracks(conferences);
		writer.write(tracks);
		return tracks;
	}
}
