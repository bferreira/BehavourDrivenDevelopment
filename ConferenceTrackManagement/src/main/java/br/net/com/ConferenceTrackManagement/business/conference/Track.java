package br.net.com.ConferenceTrackManagement.business.conference;

import java.util.ArrayList;
import java.util.List;

public class Track {
	private final List<Activity> morning;
	private final List<Activity> afternoon;

	public Track(List<Activity> morning, List<Activity> afternoon) {
		this.morning = morning;
		this.afternoon = afternoon;
	}

	public List<Activity> getMorning() {
		return morning;
	}

	public List<Activity> getAfternoon() {
		return afternoon;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Activity activity : morning) {
			sb.append(activity);
		}
		for (Activity activity : afternoon) {
			sb.append(activity);
		}
		return sb.toString();
	}

	public List<Activity> getAllActivities() {
		List<Activity> activities = new ArrayList<>();
		activities.addAll(this.getMorning());
		activities.addAll(this.getAfternoon());
		return activities;
	}
}
