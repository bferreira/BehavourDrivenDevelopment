package br.net.com.ConferenceTrackManagement.business.commandline;

import java.util.HashMap;
import java.util.Map;

import br.net.com.ConferenceTrackManagement.exceptions.InvalidCommandLineArgumentException;



/**
 * Parse command line arguments divided by the occurrence of ":" <br>
 * For instance, this command line: <br> 
 * java -jar thisjar.jar inputType:db url:http://192.165.2.1:3421/schemaX username:123 password:123 <br> 
 * Will result in this:<br> 
 * {"inputType", "db"} <br>
 * {"url", "http://192.165.2.1:3421/schemaX"} <br>
 * {"username", "123"} {"password", "123"} <br>
 * @param commandline args
 * @author bruno.ferreira.silva
 */
public class OptionsParser {
	Map<String, String> commandLineArguments;

	public OptionsParser(String[] args) {
		this.commandLineArguments = parseArgsToHashMap(args);
	}

	private Map<String, String> parseArgsToHashMap(String[] args) {
		Map<String, String> argsMap = new HashMap<>();
		for (String arg : args) {
			String[] keyValue = arg.split(":", 2);
			argsMap.put(keyValue[0], keyValue[1]);
		}
		return argsMap;
	}

	public String get(String key) {
		return get(key, null);

	}
	
	public String get(String key, String notFoundExceptionMessage) {
		String value = commandLineArguments.get(key);
		if (notFoundExceptionMessage != null && value == null) {
			throw new InvalidCommandLineArgumentException(notFoundExceptionMessage);
		}
		return value;

	}
}
