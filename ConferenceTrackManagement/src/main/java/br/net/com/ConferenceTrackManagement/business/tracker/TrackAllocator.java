package br.net.com.ConferenceTrackManagement.business.tracker;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.net.com.ConferenceTrackManagement.business.conference.Activity;
import br.net.com.ConferenceTrackManagement.business.conference.ConferenceSchedule;
import br.net.com.ConferenceTrackManagement.business.conference.LunchTime;
import br.net.com.ConferenceTrackManagement.business.conference.NetworkingEvent;
import br.net.com.ConferenceTrackManagement.business.conference.Track;

/**
 * Entity responsible for allocation of conferences in a track.<br>
 * 
 * @author bruno.ferreira.silva
 */
public class TrackAllocator {
	private static final LocalTime AFTERNOON_START_TIME = LocalTime.of(13, 00);
	private static final LocalTime START_TIME = LocalTime.of(9, 00);

	/**
	 * Create a track by allocating conferences in this windows:
	 * <p>
	 * Morning: 09:00 to 12:00<br>
	 * Afternoon: 13:00 to 17:00
	 * <p>
	 * As well allocate intervals activities:<br>
	 * lunch time: 12:00 to 13:00<br>
	 * Networking Event: 16:00 or 17:00 (earliest if possible)
	 * <p>
	 * The criteria used in this allocator to fill the track <br>
	 * is the highest sum combination of all available options
	 * <p>
	 * OBS: Allocated conferences are removed from the conferences list to avoid
	 * duplication
	 * 
	 * @param conferences
	 * @return Track with all conferences and intervals
	 */
	public Track allocate(List<Activity> conferences) {
		List<Activity> morning = processAllocation(conferences, START_TIME, 180);
		morning.add(new LunchTime());
		List<Activity> afternoon = processAllocation(conferences, AFTERNOON_START_TIME, 240);
		decideNetWorkingEventTime(afternoon);
		return new Track(morning, afternoon);
	}

	private void decideNetWorkingEventTime(List<Activity> afternoonConferences) {
		boolean shouldBeEarlist = true;
		if (afternoonConferences.isEmpty()) {
			shouldBeEarlist = true;
		} else {
			Activity lastConferenceActivity = afternoonConferences.get(afternoonConferences.size() - 1);
			LocalTime lastActivityEndTime = lastConferenceActivity.getStartTime()
					.plusMinutes(lastConferenceActivity.getDuration());
			shouldBeEarlist = lastActivityEndTime.isBefore(LocalTime.of(16, 00));
		}
		afternoonConferences.add(new NetworkingEvent(shouldBeEarlist));
	}

	private List<Activity> processAllocation(List<Activity> avaliableConferences, LocalTime scheduleTime,
			int minutesAvaliable) {
		List<Activity> consolidedConferences = allocateByHighestCombiniationOfDuration(avaliableConferences,
				minutesAvaliable);
		avaliableConferences.removeAll(consolidedConferences);
		fillStartTimeOfConferences(scheduleTime, consolidedConferences);
		return consolidedConferences;
	}

	private List<Activity> allocateByHighestCombiniationOfDuration(List<Activity> avaliableConferences,
			int minutesAvaliable) {
		// List of conferences indexed by Allocation sum
		Map<Integer, List<Activity>> candidateConferences = new HashMap<>();
		Set<Activity> listOfConferencesFromCurrentSum = new HashSet<>();
		Set<Integer> allAllocations = new HashSet<>();
		Set<Integer> recordOfHighestAllocation = new HashSet<>();
		// trick to make one number be considered a sum and be processed
		// like 0 + 7 = 7(so raw 7 will be considered as an allocation)
		allAllocations.add(0);
		int highestAllocation = 0;
		for (Activity conference : avaliableConferences) {
			Integer duration = conference.getDuration();
			if (isPerfectFit(duration, minutesAvaliable)) return new ArrayList<>(Arrays.asList(conference));
			if (duration > minutesAvaliable) break;
			for (Integer allocation : allAllocations) {
				int totalAllocation = allocation + duration;
				if (totalAllocation <= minutesAvaliable) {
					if (totalAllocation > highestAllocation) {
						recordOfHighestAllocation.add(totalAllocation);
						highestAllocation = totalAllocation;
						List<Activity> newConferencesIndex = createNewConferenceIndex(candidateConferences,
								highestAllocation, conference, allocation);
						if (isPerfectFit(highestAllocation, minutesAvaliable)) {
							return newConferencesIndex;
						} else {
							candidateConferences.put(highestAllocation, newConferencesIndex);
						}
					}
				} else {
					listOfConferencesFromCurrentSum.clear();
				}
			}
			allAllocations.addAll(recordOfHighestAllocation);
			recordOfHighestAllocation.clear();
		}
		return candidateConferences.get(highestAllocation);
	}

	private List<Activity> createNewConferenceIndex(Map<Integer, List<Activity>> candidateConferences,
			int highestAllocation, Activity conference, Integer allocation) {
		List<Activity> indexedConferences = candidateConferences.getOrDefault(allocation, new ArrayList<Activity>());
		List<Activity> newConferencesIndex = new ArrayList<Activity>(indexedConferences);
		newConferencesIndex.add(conference);
		return newConferencesIndex;
	}

	private boolean isPerfectFit(int duration, int minutesAvaliable) {
		return duration == minutesAvaliable;
	}

	private void fillStartTimeOfConferences(LocalTime scheduleTime, List<Activity> conferences) {
		// Order by title to ensure the order, so that tests don't complain :)
		conferences.sort((Activity a, Activity b) -> a.getTitle().compareTo(b.getTitle()));
		for (Activity conferenceActivity : conferences) {
			conferenceActivity.setStartTime(scheduleTime);
			scheduleTime = scheduleTime.plusMinutes(conferenceActivity.getDuration());
		}
	}

	public ConferenceSchedule getTracks(List<Activity> conferences) {
		List<Track> tracks = new ArrayList<>();
		tracks.add(allocate(conferences));
		tracks.add(allocate(conferences));
		return new ConferenceSchedule(tracks);
	}

}
