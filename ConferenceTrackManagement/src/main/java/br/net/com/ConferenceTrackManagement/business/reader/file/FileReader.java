package br.net.com.ConferenceTrackManagement.business.reader.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.net.com.ConferenceTrackManagement.business.conference.Conference;
import br.net.com.ConferenceTrackManagement.business.conference.Activity;
import br.net.com.ConferenceTrackManagement.business.reader.Reader;

public class FileReader implements Reader {
	private static final int NEXT_INDEX = 1;
	private static final int BEGIN_INDEX = 0;
	private String resourcePath;

	public FileReader(String resourcePath) throws IllegalArgumentException {
		if (resourcePath == null) {
			throw new IllegalArgumentException("resourcePath is null");
		}
		this.resourcePath = resourcePath;
	}

	public List<Activity> read() throws IOException {
		try (Stream<String> stream = Files.lines(Paths.get(resourcePath))) {
			List<Activity> list = stream.map(line -> {
				int lastBlankIndex = line.lastIndexOf(" ");
				String title = line.substring(BEGIN_INDEX, lastBlankIndex);
				Integer duration = parseDuration(line.substring(lastBlankIndex+NEXT_INDEX));
				Conference conference = new Conference(title,duration);
				return conference;
			}).collect(Collectors.toList());
			return list;
		}
	}

	private Integer parseDuration(String time) {
		Integer numTime = null;
		if (time.equals("lightning")) {
			numTime = 5;
		} else {
			numTime = Integer.parseInt(time.substring(0, time.length() - 3));
		}
		return numTime;
	}
}
