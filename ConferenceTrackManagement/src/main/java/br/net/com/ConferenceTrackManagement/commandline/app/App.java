package br.net.com.ConferenceTrackManagement.commandline.app;

import br.net.com.ConferenceTrackManagement.ConferenceTrackManager;
import br.net.com.ConferenceTrackManagement.business.commandline.OptionsParser;
import br.net.com.ConferenceTrackManagement.business.conference.ConferenceSchedule;
import br.net.com.ConferenceTrackManagement.business.reader.Reader;
import br.net.com.ConferenceTrackManagement.business.reader.ReaderFactory;
import br.net.com.ConferenceTrackManagement.business.writer.Writer;
import br.net.com.ConferenceTrackManagement.business.writer.WriterFactory;

public class App {
	private String[] args;

	public App(String[] args) throws Exception{
		this.args = args;
	}
	
	public ConferenceSchedule launch() throws Exception{
		OptionsParser options = new OptionsParser(args);
		Reader reader = new ReaderFactory().get(options);
		Writer writer = new WriterFactory().get(options);
		return new ConferenceTrackManager(reader,writer).getSchedule();
	}
	
	public static void main( String[] args ) throws Exception{
		new App(args).launch();
    }
}
