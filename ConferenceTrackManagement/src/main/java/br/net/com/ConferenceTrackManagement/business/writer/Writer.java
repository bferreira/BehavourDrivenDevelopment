package br.net.com.ConferenceTrackManagement.business.writer;

import br.net.com.ConferenceTrackManagement.business.conference.ConferenceSchedule;

public interface Writer {
	public void write(ConferenceSchedule consolidedConferenceSchedule);
}
