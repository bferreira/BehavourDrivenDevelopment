package br.net.com.ConferenceTrackManagement.business.conference;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Activity {
	private String title;
	private LocalTime startTime;
	private Integer duration;

	public Activity(String title,Integer duration) {
		this.title = title;
		this.duration = duration;
	}

	public Activity(String title, LocalTime startTime) {
		this.title = title;
		this.startTime = startTime;
	}

	public String getTitle() {
		return title;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("hh:mma");
		sb.append(fmt.format(startTime)).append(" ").append(title);
		if(duration != null){
			sb.append(" ").append(duration).append("min");
		}
		sb.append("\n");
		return sb.toString();
	}
	
}
