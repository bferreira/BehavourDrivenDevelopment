package bdd.acceptance.conferencetrackmanangement;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;

import br.net.com.ConferenceTrackManagement.business.conference.Activity;
import br.net.com.ConferenceTrackManagement.business.conference.ConferenceSchedule;
import br.net.com.ConferenceTrackManagement.business.conference.Track;
import br.net.com.ConferenceTrackManagement.commandline.app.App;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class ConferenceTrackManangmentSteps {
	private ConferenceSchedule schedule;

	@Given("^User Launch Conference Track Management with commandline \"([^\"]*)\"$")
	public void user_Launch_Conference_Track_Management_with_input(String commandLine) throws Throwable {
		schedule = new App(commandLine.split(" ")).launch();
	}

	@Then("^the Track \"([^\"]*)\" output should be:$")
	public void the_Track_output_should_be(Integer trackNumber, List<String> tracks) throws Throwable {
		int zeroBasedTrackNumber = trackNumber-1;
		Track track = schedule.getTracks().get(zeroBasedTrackNumber);
		List<Activity> allActivities = track.getAllActivities();
		for (int index = 0; index < tracks.size();index++) {
			String expected = allActivities.get(index).toString().trim();
			String actual = tracks.get(index).trim();
			assertEquals(expected,actual);
		}
	}
	
}
