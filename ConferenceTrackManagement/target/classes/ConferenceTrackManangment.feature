Feature: Conference Track Management

#You are planning a big programming conference and have received many proposals 
#which have passed the initial screen process but you're having trouble fitting 
#them into the time constraints of the day -- there are so many possibilities! 
#So you write a program to do it for you.

#The conference has multiple tracks each of which has a morning and afternoon session.
#Each session contains multiple talks.
#Morning sessions begin at 9am and must finish by 12 noon|for lunch.
#Afternoon sessions begin at 1pm and must finish in time for the networking event.
#The networking event can start no earlier than 4:00 and no later than 5:00.
#No talk title has numbers in it.
#All talk lengths are either in minutes (not hours) or lightning (5 minutes).
#Presenters will be very punctual; there needs to be no gap between sessions.
#Note that depending on how you choose to complete this problem|your solution may give
#a different ordering or combination of talks into tracks. This is acceptable; 
#you don’t need to exactly duplicate the sample output given here.

Scenario: Successful launch Conference Track Management with .txt input

Given User Launch Conference Track Management with commandline "inputType:file writerType:console path:src/test/resources/halfAllocation.txt"
Then the Track "1" output should be:
|09:00AM Lua for the Masses 30min|
|09:30AM Overdoing it in Python 45min|
|10:15AM Ruby Errors from Mismatched Gem Versions 45min|
|11:00AM Writing Fast Tests Against Enterprise Rails 60min|
|12:00PM Lunch|
|01:00PM Accounting-Driven Development 45min|
|01:45PM Common Ruby Errors 45min|
|02:30PM Communicating Over Distance 60min|
|03:30PM Pair Programming vs Noise 45min|
|04:15PM Rails for Python Developers 5min|
|04:20PM Woah 30min|
|05:00PM Networking Event|
Then the Track "2" output should be:
|09:00AM Programming in the Boondocks of Seattle 30min|
|09:30AM Rails Magic 60min|
|10:30AM Ruby on Rails: Why We Should Move On 60min|
|11:30AM Sit Down and Write 30min|
|12:00PM Lunch|
|01:00PM A World Without HackerNews 30min|
|01:30PM Clojure Ate Scala (on my project) 45min|
|02:15PM Ruby on Rails Legacy App Maintenance 60min|
|03:15PM Ruby vs. Clojure for Back-End Development 30min|
|03:45PM User Interface CSS in Rails Apps 30min|
|05:00PM Networking Event|

Given User Launch Conference Track Management with commandline "inputType:file writerType:console path:src/test/resources/overFullAlocation.txt"
Then the Track "1" output should be:
|09:00AM morning1 180min|
|12:00PM Lunch|
|01:00PM afternoon1 240min|
|05:00PM Networking Event|
Then the Track "2" output should be:
|09:00AM morning2 180min|
|12:00PM Lunch|
|01:00PM afternoon2 240min|
|05:00PM Networking Event|

Given User Launch Conference Track Management with commandline "inputType:file writerType:console path:src/test/resources/fullAllocation.txt"
Then the Track "1" output should be:
|09:00AM Communicating Over Distance 60min|
|10:00AM Writing Fast Tests Against Enterprise Rails 60min|
|11:00AM im here just to make Full House :D 55min|
|12:00PM Lunch|
|01:00PM Accounting-Driven Development 45min|
|01:45PM Common Ruby Errors 45min|
|02:30PM Lua for the Masses 30min|
|03:00PM Overdoing it in Python 45min|
|03:45PM Ruby Errors from Mismatched Gem Versions 45min|
|04:30PM Woah 30min|
|05:00PM Networking Event|
Then the Track "2" output should be:
|09:00AM Pair Programming vs Noise 45min|
|09:45AM Programming in the Boondocks of Seattle 30min|
|10:15AM Rails Magic 60min|
|11:15AM Rails for Python Developers 5min|
|11:20AM Sit Down and Write 30min|
|12:00PM Lunch|
|01:00PM A World Without HackerNews 30min|
|01:30PM Clojure Ate Scala (on my project) 45min|
|02:15PM Ruby on Rails Legacy App Maintenance 60min|
|03:15PM Ruby on Rails: Why We Should Move On 60min|
|04:15PM Ruby vs. Clojure for Back-End Development 30min|
|05:00PM Networking Event|












	
	

