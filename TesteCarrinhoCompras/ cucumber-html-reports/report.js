$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("adicionarItem.feature");
formatter.feature({
  "line": 1,
  "name": "Adicionar item ao carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 3,
      "value": "# Permite a adição de um novo item no carrinho de compras."
    },
    {
      "line": 4,
      "value": "#"
    },
    {
      "line": 5,
      "value": "# Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser"
    },
    {
      "line": 6,
      "value": "# seguidas: - A quantidade do item deverá ser a soma da quantidade atual com a quantidade"
    },
    {
      "line": 7,
      "value": "# passada como parâmetro. - Se o valor unitário informado for diferente do valor unitário atual"
    },
    {
      "line": 8,
      "value": "# do item, o novo valor unitário do item deverá ser o passado como parâmetro."
    }
  ],
  "line": 10,
  "name": "Adicionar um novo item no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni\u003e quantidade \u003cqtdprod\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "carrinho deverá possuir produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni\u003e quantidade \u003cqtdprod\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "carrinho deverá possuir valor total de \u003ctotalcar\u003e e quantidade de items 1",
  "keyword": "Then "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;",
  "rows": [
    {
      "cells": [
        "codprod",
        "descprod",
        "valuni",
        "qtdprod",
        "totalcar"
      ],
      "line": 16,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;1"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "1",
        "3.75"
      ],
      "line": 17,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;2"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "2",
        "7.50"
      ],
      "line": 18,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;3"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "3",
        "11.25"
      ],
      "line": 19,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;4"
    },
    {
      "cells": [
        "1",
        "tv",
        "123",
        "1",
        "123.0"
      ],
      "line": 20,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;5"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 17,
  "name": "Adicionar um novo item no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 1",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 1",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "carrinho deverá possuir valor total de 3.75 e quantidade de items 1",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 219125974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "1",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 15581525,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3.75",
      "offset": 80
    },
    {
      "val": "1",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 780717,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3.75",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 253538,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Adicionar um novo item no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "carrinho deverá possuir valor total de 7.50 e quantidade de items 1",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 57846,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 274461,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3.75",
      "offset": 80
    },
    {
      "val": "2",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 341743,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "7.50",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 189538,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Adicionar um novo item no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 3",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 3",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "carrinho deverá possuir valor total de 11.25 e quantidade de items 1",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 43897,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 271179,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3.75",
      "offset": 80
    },
    {
      "val": "3",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 280616,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "11.25",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 67
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 139487,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Adicionar um novo item no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-novo-item-no-carrinho-de-compras;;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "o usuário adicionar o produto de código: 1 descrição: tv valor unitário: 123 quantidade 1",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "carrinho deverá possuir produto de código: 1 descrição: tv valor unitário: 123 quantidade 1",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "carrinho deverá possuir valor total de 123.0 e quantidade de items 1",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 42666,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "tv",
      "offset": 54
    },
    {
      "val": "123",
      "offset": 73
    },
    {
      "val": "1",
      "offset": 88
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 260102,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "tv",
      "offset": 56
    },
    {
      "val": "123",
      "offset": 75
    },
    {
      "val": "1",
      "offset": 90
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 243282,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123.0",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 67
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 119794,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 22,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni1\u003e quantidade \u003cqtdprod1\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni2\u003e quantidade \u003cqtdprod2\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "carrinho deverá possuir produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni2\u003e quantidade \u003cqtdprodesperado\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "carrinho deverá possuir valor total de \u003ctotalcar\u003e e quantidade de items 1",
  "keyword": "Then "
});
formatter.examples({
  "line": 29,
  "name": "",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;",
  "rows": [
    {
      "cells": [
        "codprod",
        "descprod",
        "valuni1",
        "valuni2",
        "qtdprod1",
        "qtdprod2",
        "qtdprodesperado",
        "totalcar"
      ],
      "line": 30,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;1"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "3.75",
        "2",
        "3",
        "5",
        "18.75"
      ],
      "line": 31,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;2"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "4.50",
        "2",
        "3",
        "5",
        "22.5"
      ],
      "line": 32,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;3"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "3",
        "2",
        "3",
        "5",
        "15.0"
      ],
      "line": 33,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 31,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 3",
  "matchedColumns": [
    0,
    1,
    3,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 5",
  "matchedColumns": [
    0,
    1,
    3,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "carrinho deverá possuir valor total de 18.75 e quantidade de items 1",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 32410,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 176821,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 202667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3.75",
      "offset": 80
    },
    {
      "val": "5",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 282257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "18.75",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 67
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 159589,
  "status": "passed"
});
formatter.scenario({
  "line": 32,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 4.50 quantidade 3",
  "matchedColumns": [
    0,
    1,
    3,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 4.50 quantidade 5",
  "matchedColumns": [
    0,
    1,
    3,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "carrinho deverá possuir valor total de 22.5 e quantidade de items 1",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 53333,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 228923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "4.50",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 224000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "4.50",
      "offset": 80
    },
    {
      "val": "5",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 516102,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "22.5",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 123077,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3 quantidade 3",
  "matchedColumns": [
    0,
    1,
    3,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3 quantidade 5",
  "matchedColumns": [
    0,
    1,
    3,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "carrinho deverá possuir valor total de 15.0 e quantidade de items 1",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 45129,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 266256,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 91
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 253128,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3",
      "offset": 80
    },
    {
      "val": "5",
      "offset": 93
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 278154,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "15.0",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 113231,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 35,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "  Given",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 37,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni1\u003e quantidade \u003cqtdprod1\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni2\u003e quantidade \u003cqtdprod2\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "carrinho deverá possuir produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni2\u003e quantidade \u003cqtdprodesperado\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "carrinho deverá possuir valor total de \u003ctotalcar\u003e e quantidade de items 1",
  "keyword": "Then "
});
formatter.examples({
  "line": 42,
  "name": "",
  "description": "",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;",
  "rows": [
    {
      "cells": [
        "codprod",
        "descprod",
        "valuni1",
        "valuni2",
        "qtdprod1",
        "qtdprod2",
        "qtdprodesperado",
        "totalcar"
      ],
      "line": 43,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;1"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "3.75",
        "2",
        "3",
        "5",
        "18.75"
      ],
      "line": 44,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;2"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "4.50",
        "2",
        "3",
        "5",
        "22.5"
      ],
      "line": 45,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;3"
    },
    {
      "cells": [
        "1",
        "relógio",
        "3.75",
        "3",
        "2",
        "3",
        "5",
        "15.0"
      ],
      "line": 46,
      "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 44,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "  Given",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 37,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 3",
  "matchedColumns": [
    0,
    1,
    3,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 5",
  "matchedColumns": [
    0,
    1,
    3,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "carrinho deverá possuir valor total de 18.75 e quantidade de items 1",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 331898,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 289641,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3.75",
      "offset": 80
    },
    {
      "val": "5",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 231384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "18.75",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 67
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 88205,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "  Given",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 37,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 4.50 quantidade 3",
  "matchedColumns": [
    0,
    1,
    3,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 4.50 quantidade 5",
  "matchedColumns": [
    0,
    1,
    3,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "carrinho deverá possuir valor total de 22.5 e quantidade de items 1",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 264205,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "4.50",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 260923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "4.50",
      "offset": 80
    },
    {
      "val": "5",
      "offset": 96
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 284718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "22.5",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 116513,
  "status": "passed"
});
formatter.scenario({
  "line": 46,
  "name": "Adicionar um item já existente no carrinho de compras",
  "description": "  Given",
  "id": "adicionar-item-ao-carrinho-de-compras;adicionar-um-item-já-existente-no-carrinho-de-compras;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 37,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 2",
  "matchedColumns": [
    0,
    1,
    2,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3 quantidade 3",
  "matchedColumns": [
    0,
    1,
    3,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "carrinho deverá possuir produto de código: 1 descrição: relógio valor unitário: 3 quantidade 5",
  "matchedColumns": [
    0,
    1,
    3,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "carrinho deverá possuir valor total de 15.0 e quantidade de items 1",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "2",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 327384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3",
      "offset": 78
    },
    {
      "val": "3",
      "offset": 91
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 693332,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    },
    {
      "val": "relógio",
      "offset": 56
    },
    {
      "val": "3",
      "offset": 80
    },
    {
      "val": "5",
      "offset": 93
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 234667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "15.0",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 148923,
  "status": "passed"
});
formatter.uri("criarCarrinho.feature");
formatter.feature({
  "line": 1,
  "name": "Criar carrinho de compras",
  "description": "",
  "id": "criar-carrinho-de-compras",
  "keyword": "Feature"
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "# Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro."
    },
    {
      "line": 4,
      "value": "#"
    },
    {
      "line": 5,
      "value": "# Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho"
    },
    {
      "line": 6,
      "value": "# deverá ser retornado."
    }
  ],
  "line": 8,
  "name": "cria um carrinho de compras para o cliente",
  "description": "",
  "id": "criar-carrinho-de-compras;cria-um-carrinho-de-compras-para-o-cliente",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "não existe carrinho criado para o usuário de cpf 01422389456",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "criar o carrinho de compras para o usuário de cpf 01422389456",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "deverá existir carrinho de compras para o usuário de cpf 01422389456",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 49
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:22"
});
formatter.result({
  "duration": 78769,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 50
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:28"
});
formatter.result({
  "duration": 476717,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 57
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:33"
});
formatter.result({
  "duration": 56205,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "tentar criar um carrinho de compras para o cliente que já possui de modo a reaproveitar o carrinho existente",
  "description": "",
  "id": "criar-carrinho-de-compras;tentar-criar-um-carrinho-de-compras-para-o-cliente-que-já-possui-de-modo-a-reaproveitar-o-carrinho-existente",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "já existe carrinho criado para o usuário de cpf 01422389456",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "o cliente de cpf 01422389456 deverá existir carrinho de compras com o produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "2",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 17
    },
    {
      "val": "1",
      "offset": 89
    },
    {
      "val": "relógio",
      "offset": 102
    },
    {
      "val": "2",
      "offset": 126
    },
    {
      "val": "1",
      "offset": 139
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:46"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("invalidarCarrinho.feature");
formatter.feature({
  "line": 1,
  "name": "Criar carrinho de compras",
  "description": "\r\n* Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar. Deve\r\n* ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos\r\n* de compras.",
  "id": "criar-carrinho-de-compras",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 7,
  "name": "invalidar carrinho de compras do cliente",
  "description": "",
  "id": "criar-carrinho-de-compras;invalidar-carrinho-de-compras-do-cliente",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "já existe carrinho criado para o usuário de cpf 01422389456",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "invalidar o carrinho de compras para o usuário de cpf 01422389456",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "não deverá existir carrinho de compras para o usuário de cpf 01422389456",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 54
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:57"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 61
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:61"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("removerItem.feature");
formatter.feature({
  "line": 1,
  "name": "Remover item do carrinho de compras",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 3,
      "value": "# Permite a remoção do item que representa este produto do carrinho de compras."
    },
    {
      "line": 4,
      "value": "# @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e"
    },
    {
      "line": 5,
      "value": "#         false caso o produto não exista no carrinho."
    }
  ],
  "line": 7,
  "name": "Remover item pelo código do produto",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni\u003e quantidade \u003cqtdprod\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "o usuário remove o produto de codigo \u003ccodprodremover\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "carrinho deverá possuir valor total de \u003ctotalcar\u003e e quantidade de items \u003cqtdprod\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto;",
  "rows": [
    {
      "cells": [
        "codprod",
        "codprodremover",
        "descprod",
        "valuni",
        "qtdprod",
        "totalcar"
      ],
      "line": 13,
      "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto;;1"
    },
    {
      "cells": [
        "1",
        "1",
        "relógio",
        "3.75",
        "0",
        "0"
      ],
      "line": 14,
      "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto;;2"
    },
    {
      "cells": [
        "1",
        "5",
        "relógio",
        "3.75",
        "1",
        "3.75"
      ],
      "line": 15,
      "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "Remover item pelo código do produto",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 0",
  "matchedColumns": [
    0,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "o usuário remove o produto de codigo 1",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "carrinho deverá possuir valor total de 0 e quantidade de items 0",
  "matchedColumns": [
    4,
    5
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 45128,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "0",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 768820,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 37
    }
  ],
  "location": "CarrinhoComprasTest.removeItem(Long)"
});
formatter.result({
  "duration": 695384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 39
    },
    {
      "val": "0",
      "offset": 63
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 120615,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Remover item pelo código do produto",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-código-do-produto;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 1",
  "matchedColumns": [
    0,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "o usuário remove o produto de codigo 5",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "carrinho deverá possuir valor total de 3.75 e quantidade de items 1",
  "matchedColumns": [
    4,
    5
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 66462,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "1",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 239179,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 37
    }
  ],
  "location": "CarrinhoComprasTest.removeItem(Long)"
});
formatter.result({
  "duration": 148103,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3.75",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 116513,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 17,
  "name": "Remover item pelo indice produto",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 18,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "o usuário adicionar o produto de código: \u003ccodprod\u003e descrição: \u003cdescprod\u003e valor unitário: \u003cvaluni\u003e quantidade \u003cqtdprod\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "o usuário remove o produto de indice \u003cindice\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "carrinho deverá possuir valor total de \u003ctotalcar\u003e e quantidade de items \u003cqtdprod\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 22,
  "name": "",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto;",
  "rows": [
    {
      "cells": [
        "codprod",
        "indice",
        "descprod",
        "valuni",
        "qtdprod",
        "totalcar"
      ],
      "line": 23,
      "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto;;1"
    },
    {
      "cells": [
        "1",
        "0",
        "relógio",
        "3.75",
        "0",
        "0"
      ],
      "line": 24,
      "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto;;2"
    },
    {
      "cells": [
        "1",
        "2",
        "relógio",
        "3.75",
        "1",
        "3.75"
      ],
      "line": 25,
      "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 24,
  "name": "Remover item pelo indice produto",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 18,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 0",
  "matchedColumns": [
    0,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "o usuário remove o produto de indice 0",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "carrinho deverá possuir valor total de 0 e quantidade de items 0",
  "matchedColumns": [
    4,
    5
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 42666,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "0",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 226871,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 37
    }
  ],
  "location": "CarrinhoComprasTest.removeItem(Integer)"
});
formatter.result({
  "duration": 276102,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 39
    },
    {
      "val": "0",
      "offset": 63
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 131282,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Remover item pelo indice produto",
  "description": "",
  "id": "remover-item-do-carrinho-de-compras;remover-item-pelo-indice-produto;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 18,
  "name": "o carrinho esta vazio",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "o usuário adicionar o produto de código: 1 descrição: relógio valor unitário: 3.75 quantidade 1",
  "matchedColumns": [
    0,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "o usuário remove o produto de indice 2",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "carrinho deverá possuir valor total de 3.75 e quantidade de items 1",
  "matchedColumns": [
    4,
    5
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "CarrinhoComprasTest.instanciarCarrinho()"
});
formatter.result({
  "duration": 53743,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 41
    },
    {
      "val": "relógio",
      "offset": 54
    },
    {
      "val": "3.75",
      "offset": 78
    },
    {
      "val": "1",
      "offset": 94
    }
  ],
  "location": "CarrinhoComprasTest.adicionarProduto(Long,String,BigDecimal,int)"
});
formatter.result({
  "duration": 249846,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 37
    }
  ],
  "location": "CarrinhoComprasTest.removeItem(Integer)"
});
formatter.result({
  "duration": 185026,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3.75",
      "offset": 39
    },
    {
      "val": "1",
      "offset": 66
    }
  ],
  "location": "CarrinhoComprasTest.validarSeProdutoExisteNoCarrinho(String,int)"
});
formatter.result({
  "duration": 411897,
  "status": "passed"
});
formatter.uri("ticketMedio.feature");
formatter.feature({
  "line": 1,
  "name": "Criar carrinho de compras",
  "description": "",
  "id": "criar-carrinho-de-compras",
  "keyword": "Feature"
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "# Retorna o valor do ticket médio no momento da chamada ao método. O valor do ticket médio é a"
    },
    {
      "line": 4,
      "value": "# soma do valor total de todos os carrinhos de compra dividido pela quantidade de carrinhos de"
    },
    {
      "line": 5,
      "value": "# compra. O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:"
    },
    {
      "line": 6,
      "value": "# 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima."
    }
  ],
  "line": 8,
  "name": "calcular ticket médio",
  "description": "",
  "id": "criar-carrinho-de-compras;calcular-ticket-médio",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "adicionar no carrinho do cliente 12345678910 produto de código: 1 descrição: relógio valor unitário: 10 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "o ticket médio deverá ser de 6.00 reais",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "2",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 278974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "12345678910",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "10",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 115
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 240820,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6.00",
      "offset": 29
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:65"
});
formatter.result({
  "duration": 130051,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "calcular ticket médio roundBottom 0-4",
  "description": "",
  "id": "criar-carrinho-de-compras;calcular-ticket-médio-roundbottom-0-4",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "adicionar no carrinho do cliente 12345678910 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "adicionar no carrinho do cliente 45465456456 produto de código: 1 descrição: relógio valor unitário: 6 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "o ticket médio deverá ser de 3.33 reais",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "2",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 208000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "12345678910",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "2",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 167795,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "45465456456",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "6",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 224000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3.33",
      "offset": 29
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:65"
});
formatter.result({
  "duration": 106667,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "calcular ticket médio roundTop 5-9",
  "description": "",
  "id": "criar-carrinho-de-compras;calcular-ticket-médio-roundtop-5-9",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "adicionar no carrinho do cliente 12345678910 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "adicionar no carrinho do cliente 45465456456 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "adicionar no carrinho do cliente 12312312333 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "adicionar no carrinho do cliente 67676767677 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "adicionar no carrinho do cliente 34344343443 produto de código: 1 descrição: relógio valor unitário: 5 quantidade 1",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "o ticket médio deverá ser de 1.67 reais",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "01422389456",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "1",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 269128,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "12345678910",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "1",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 356513,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "45465456456",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "1",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 235487,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "12312312333",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "1",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 967384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "67676767677",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "1",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 213744,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "34344343443",
      "offset": 33
    },
    {
      "val": "1",
      "offset": 64
    },
    {
      "val": "relógio",
      "offset": 77
    },
    {
      "val": "5",
      "offset": 101
    },
    {
      "val": "1",
      "offset": 114
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:38"
});
formatter.result({
  "duration": 226872,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1.67",
      "offset": 29
    }
  ],
  "location": "CarrinhoComprasFactorySteps.java:65"
});
formatter.result({
  "duration": 109128,
  "status": "passed"
});
});