Feature: Criar carrinho de compras

	 * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar. Deve
	 * ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos
	 * de compras.
	 
    Scenario: invalidar carrinho de compras do cliente
        Given já existe carrinho criado para o usuário de cpf 01422389456
        When invalidar o carrinho de compras para o usuário de cpf 01422389456
        Then não deverá existir carrinho de compras para o usuário de cpf 01422389456