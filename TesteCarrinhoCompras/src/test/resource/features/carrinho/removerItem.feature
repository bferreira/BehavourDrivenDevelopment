Feature: Remover item do carrinho de compras
	 
	 # Permite a remoção do item que representa este produto do carrinho de compras.
	 # @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 #         false caso o produto não exista no carrinho.
	 
    Scenario Outline: Remover item pelo código do produto
        Given o carrinho esta vazio
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni> quantidade <qtdprod>
        Then o usuário remove o produto de codigo <codprodremover>
        Then carrinho deverá possuir valor total de <totalcar> e quantidade de items <qtdprod>
        Examples:
		    | codprod  | codprodremover | descprod | valuni | qtdprod | totalcar | 
		    |    1 	   |    1 	        | relógio  |  3.75  |    0	  |    0     |       
		    |    1 	   |    5 	        | relógio  |  3.75  |    1	  |    3.75  | 
		          
    Scenario Outline: Remover item pelo indice produto
        Given o carrinho esta vazio
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni> quantidade <qtdprod>
        Then o usuário remove o produto de indice <indice>
        Then carrinho deverá possuir valor total de <totalcar> e quantidade de items <qtdprod>
        Examples:
		    | codprod  | indice | descprod | valuni | qtdprod | totalcar | 
		    |    1 	   |    0 	| relógio  |  3.75  |    0	  |    0     |       
		    |    1 	   |    2 	| relógio  |  3.75  |    1	  |    3.75  |       
		       
