Feature: Criar carrinho de compras

	 # Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
	 #
	 # Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho
	 # deverá ser retornado.
	 
    Scenario: cria um carrinho de compras para o cliente
        Given não existe carrinho criado para o usuário de cpf 01422389456 
        When criar o carrinho de compras para o usuário de cpf 01422389456
        Then deverá existir carrinho de compras para o usuário de cpf 01422389456
        
    Scenario: tentar criar um carrinho de compras para o cliente que já possui de modo a reaproveitar o carrinho existente
        Given já existe carrinho criado para o usuário de cpf 01422389456 
        When adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1
        Then o cliente de cpf 01422389456 deverá existir carrinho de compras com o produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1