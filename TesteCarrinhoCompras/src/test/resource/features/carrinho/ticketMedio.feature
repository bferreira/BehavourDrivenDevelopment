Feature: Criar carrinho de compras

	 # Retorna o valor do ticket médio no momento da chamada ao método. O valor do ticket médio é a
	 # soma do valor total de todos os carrinhos de compra dividido pela quantidade de carrinhos de
	 # compra. O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
	 # 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
	 
    Scenario: calcular ticket médio
        When adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1
        When adicionar no carrinho do cliente 12345678910 produto de código: 1 descrição: relógio valor unitário: 10 quantidade 1
        Then o ticket médio deverá ser de 6.00 reais
        
    Scenario: calcular ticket médio roundBottom 0-4
        When adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1
        When adicionar no carrinho do cliente 12345678910 produto de código: 1 descrição: relógio valor unitário: 2 quantidade 1
        When adicionar no carrinho do cliente 45465456456 produto de código: 1 descrição: relógio valor unitário: 6 quantidade 1
        Then o ticket médio deverá ser de 3.33 reais
        
    Scenario: calcular ticket médio roundTop 5-9
        When adicionar no carrinho do cliente 01422389456 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1
        When adicionar no carrinho do cliente 12345678910 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1
        When adicionar no carrinho do cliente 45465456456 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1
        When adicionar no carrinho do cliente 12312312333 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1
        When adicionar no carrinho do cliente 67676767677 produto de código: 1 descrição: relógio valor unitário: 1 quantidade 1
        When adicionar no carrinho do cliente 34344343443 produto de código: 1 descrição: relógio valor unitário: 5 quantidade 1
        Then o ticket médio deverá ser de 1.67 reais