Feature: Adicionar item ao carrinho de compras

	 # Permite a adição de um novo item no carrinho de compras.
	 #
	 # Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser
	 # seguidas: - A quantidade do item deverá ser a soma da quantidade atual com a quantidade
	 # passada como parâmetro. - Se o valor unitário informado for diferente do valor unitário atual
	 # do item, o novo valor unitário do item deverá ser o passado como parâmetro.
	 
    Scenario Outline: Adicionar um novo item no carrinho de compras
        Given o carrinho esta vazio
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni> quantidade <qtdprod>
        Then carrinho deverá possuir produto de código: <codprod> descrição: <descprod> valor unitário: <valuni> quantidade <qtdprod>
        Then carrinho deverá possuir valor total de <totalcar> e quantidade de items 1
        Examples:
		    | codprod | descprod | valuni | qtdprod | totalcar | 
		    |    1 	  | relógio  |  3.75  |    1	|    3.75  |       
		    |    1 	  | relógio  |  3.75  |    2	|    7.50  |       
		    |    1 	  | relógio  |  3.75  |    3	|   11.25  |       
		    |    1 	  |  tv      | 123    |    1	|   123.0  |   
		       
    Scenario Outline: Adicionar um item já existente no carrinho de compras
        Given o carrinho esta vazio
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni1> quantidade <qtdprod1>
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni2> quantidade <qtdprod2>
        Then carrinho deverá possuir produto de código: <codprod> descrição: <descprod> valor unitário: <valuni2> quantidade <qtdprodesperado>
        Then carrinho deverá possuir valor total de <totalcar> e quantidade de items 1
        
	 	Examples:
		   | codprod | descprod | valuni1 | valuni2 | qtdprod1 | qtdprod2 | qtdprodesperado   | totalcar | 
		   |    1    | relógio  |  3.75   | 3.75    |    2     |    3	  |    5	          |   18.75  |      
		   |    1    | relógio  |  3.75   | 4.50    |    2     |    3	  |    5	          |   22.5   |       
		   |    1    | relógio  |  3.75   | 3       |    2     |    3	  |    5	          |   15.0   |  
		   
    Scenario Outline: Adicionar um item já existente no carrinho de compras
        Given
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni1> quantidade <qtdprod1>
        When o usuário adicionar o produto de código: <codprod> descrição: <descprod> valor unitário: <valuni2> quantidade <qtdprod2>
        Then carrinho deverá possuir produto de código: <codprod> descrição: <descprod> valor unitário: <valuni2> quantidade <qtdprodesperado>
        Then carrinho deverá possuir valor total de <totalcar> e quantidade de items 1
        
	 	Examples:
		   | codprod | descprod | valuni1 | valuni2 | qtdprod1 | qtdprod2 | qtdprodesperado | totalcar | 
		   |    1    | relógio  |  3.75   | 3.75    |    2     |    3	  |    5	        |   18.75  |      
		   |    1    | relógio  |  3.75   | 4.50    |    2     |    3	  |    5	        |   22.5   |       
		   |    1    | relógio  |  3.75   | 3       |    2     |    3	  |    5	        |   15.0   |