package bdd.carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;

import cucumber.api.java8.En;

import br.com.improving.carrinho.CarrinhoCompras;
import br.com.improving.carrinho.CarrinhoComprasFactory;
import br.com.improving.carrinho.Item;
import br.com.improving.carrinho.Produto;

public class CarrinhoComprasFactorySteps implements En {
	private CarrinhoComprasFactory factory = new CarrinhoComprasFactory();

	public CarrinhoComprasFactorySteps() {
		Given("^não existe carrinho criado para o usuário de cpf (\\d+)$",
				(String identificacaoCliente) -> {
					assertNull(factory.getCarrinho(identificacaoCliente));
				});

		Given("^já existe carrinho criado para o usuário de cpf (\\d+)$",
				(String identificacaoCliente) -> {
					factory.criar(identificacaoCliente);
					assertNotNull(factory.getCarrinho(identificacaoCliente));
				});

		When("^criar o carrinho de compras para o usuário de cpf (\\d+)$",
				(String identificacaoCliente) -> {
					factory.criar(identificacaoCliente);
				});

		Then("^deverá existir carrinho de compras para o usuário de cpf (\\d+)$",
				(String identificacaoCliente) -> {
					assertNotNull(factory.getCarrinho(identificacaoCliente));
				});

		When("^adicionar no carrinho do cliente (\\d+) produto de código: (\\d+) descrição: (.*) valor unitário: (.*) quantidade (\\d+)$",
				(String identificacaoCliente, Long codigo, String descricao,
						BigDecimal valorUnitario, Integer quantidade) -> {
					CarrinhoCompras carrinho = factory.criar(identificacaoCliente);
					carrinho.adicionarItem(new Produto(codigo, descricao), valorUnitario,
							quantidade);
				});

		Then("^o cliente de cpf (.*) deverá existir carrinho de compras com o produto de código: (\\d+) descrição: (.*) valor unitário: (.*) quantidade (\\d+)$",
			(String identificacaoCliente, Long codigo, String descricao,BigDecimal valorUnitario, Integer quantidade) -> {
			CarrinhoCompras carrinho = factory.getCarrinho(identificacaoCliente);
			Optional<Item> itemOptional = carrinho.getItem(codigo);
			assertTrue(itemOptional.isPresent());
			Item item = itemOptional.get();
			assertEquals(codigo, item.getProduto().getCodigo());
			assertEquals(valorUnitario, item.getValorUnitario());
			assertEquals(quantidade.intValue(), item.getQuantidade());
		});

		When("^invalidar o carrinho de compras para o usuário de cpf (\\d+)$", (String identificacaoCliente) -> {
			factory.invalidar(identificacaoCliente);
		});

		Then("^não deverá existir carrinho de compras para o usuário de cpf (\\d+)$", (String identificacaoCliente) -> {
			assertNull(factory.getCarrinho(identificacaoCliente));
		});
		
		Then("^o ticket médio deverá ser de (.*) reais$", (String ticketMedio) -> {
			assertEquals(new BigDecimal(ticketMedio), factory.getValorTicketMedio());
		});
	}
}
