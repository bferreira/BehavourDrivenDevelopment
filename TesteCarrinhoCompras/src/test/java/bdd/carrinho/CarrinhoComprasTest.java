package bdd.carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import br.com.improving.carrinho.CarrinhoCompras;
import br.com.improving.carrinho.Item;
import br.com.improving.carrinho.Produto;

public class CarrinhoComprasTest {
	private CarrinhoCompras carrinhoCompras = new CarrinhoCompras();

	@Given("^o carrinho esta vazio$")
	public void instanciarCarrinho() {
		assertTrue(carrinhoCompras.getItens().isEmpty());
	}

	@When("^o usuário adicionar o produto de código: (.*) descrição: (.*) valor unitário: (.*) quantidade (.*)$")
	public void adicionarProduto(Long codigoProduto, String nomeProduto, BigDecimal valorUnitario,
			int quantidade) {
		Produto produto = new Produto(codigoProduto, nomeProduto);
		carrinhoCompras.adicionarItem(produto, valorUnitario, quantidade);
	}

	@Then("^carrinho deverá possuir produto de código: (\\d+) descrição: (.*) valor unitário: (.*) quantidade (\\d+)$")
	public void validarSeProdutoExisteNoCarrinho(Long codigoProduto, String descProduto,
			BigDecimal valorUnitario, int quantidade) {
		Optional<Item> itemNoCarrinho = carrinhoCompras.getItens().stream()
				.filter(item -> item.getProduto().getCodigo().equals(codigoProduto)).findFirst();
		assertTrue(itemNoCarrinho.isPresent());
		Item item = itemNoCarrinho.get();
		Produto produto = item.getProduto();
		assertEquals(codigoProduto, produto.getCodigo());
		assertEquals(descProduto, produto.getDescricao());
		assertEquals(item.getQuantidade(), quantidade);
		assertEquals(item.getValorUnitario(), valorUnitario);
	}
	
	@Then("^carrinho deverá possuir valor total de (.*) e quantidade de items (\\d+)$")
	public void validarSeProdutoExisteNoCarrinho(String valorTotal, int quantidadeItems) {
		assertEquals(new BigDecimal(valorTotal),carrinhoCompras.getValorTotal());
		assertEquals(quantidadeItems,carrinhoCompras.getItens().size());
	}
	
	@Then("^o usuário remove o produto de codigo (\\d+)$")
	public void removeItem(Long codigo) {
		carrinhoCompras.removerItem(new Produto(codigo, ""));
	}
	
	@Then("^o usuário remove o produto de indice (\\d+)$")
	public void removeItem(Integer indice) {
		carrinhoCompras.removerItem(indice);
	}
}
