package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.swing.text.html.Option;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */
public class CarrinhoComprasFactory {
	private Map<String, CarrinhoCompras> carrinhosComprasManager = new HashMap<>();

	/**
	 * Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
	 *
	 * Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho
	 * deverá ser retornado.
	 *
	 * @param identificacaoCliente
	 * @return CarrinhoCompras
	 */
	public CarrinhoCompras criar(String identificacaoCliente) {
		CarrinhoCompras carrinhoCompras = new CarrinhoCompras();
		return Optional.ofNullable(carrinhosComprasManager.putIfAbsent(identificacaoCliente, carrinhoCompras)).orElseGet(()->carrinhoCompras);
	}

	/**
	 * Retorna o valor do ticket médio no momento da chamada ao método. O valor do ticket médio é a
	 * soma do valor total de todos os carrinhos de compra dividido pela quantidade de carrinhos de
	 * compra. O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
	 * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTicketMedio() {
		BigDecimal valorTotalGeral = somarValorTotalDeTodosCarrinhos();
		BigDecimal quantidadeDeCarrinhos = new BigDecimal(carrinhosComprasManager.size());
		return valorTotalGeral.divide(quantidadeDeCarrinhos,2,BigDecimal.ROUND_HALF_UP);
	}

	private BigDecimal somarValorTotalDeTodosCarrinhos() {
		BigDecimal totalCompras = new BigDecimal(0);
		for (CarrinhoCompras carrinhoCompras : carrinhosComprasManager.values()) {
			totalCompras = totalCompras.add(carrinhoCompras.getValorTotal());
		}
		return totalCompras;
	}

	/**
	 * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar. Deve
	 * ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos
	 * de compras.
	 *
	 * @param identificacaoCliente
	 * @return Retorna um boolean, tendo o valor true caso o cliente passado como parämetro tenha um
	 *         carrinho de compras e e false caso o cliente não possua um carrinho.
	 */
	public boolean invalidar(String identificacaoCliente) {
		/**
		 * Lock nesta instância com objetivo de não remover/invalidar o carrinho enquanto o
		 * carrinhosComprasManager é utilizado em outros métodos,assim ,evitando inconsistências.
		 */
		synchronized (this) {
			boolean clientePossuiCarrinhoComprasCriado =
					carrinhosComprasManager.containsKey(identificacaoCliente);
			if (clientePossuiCarrinhoComprasCriado) {
				carrinhosComprasManager.remove(identificacaoCliente);
			}
			return clientePossuiCarrinhoComprasCriado;
		}
	}

	public CarrinhoCompras getCarrinho(String identificacaoCliente) {
		return carrinhosComprasManager.get(identificacaoCliente);
	}
}
