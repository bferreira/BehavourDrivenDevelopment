package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import br.com.improving.carrinho.exceptions.AdicionarItemException;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras {
	private List<Item> items = new ArrayList<>();

	/**
	 * Permite a adição de um novo item no carrinho de compras.
	 *
	 * Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser
	 * seguidas: - A quantidade do item deverá ser a soma da quantidade atual com a quantidade
	 * passada como parâmetro. - Se o valor unitário informado for diferente do valor unitário atual
	 * do item, o novo valor unitário do item deverá ser o passado como parâmetro.
	 *
	 * Devem ser lançadas subclasses de RuntimeException caso não seja possível adicionar o item ao
	 * carrinho de compras.
	 *
	 * @param produto
	 * @param valorUnitario
	 * @param quantidade
	 */
	public void adicionarItem(Produto produto, BigDecimal valorUnitario, int quantidade) {
		try {
			Optional<Item> itemNoCarrinho =
					items.stream().filter(item -> item.getProduto().equals(produto)).findFirst();
			if (itemNoCarrinho.isPresent()) {
				Item item = itemNoCarrinho.get();
				item.setValorUnitario(valorUnitario);
				item.setQuantidade(item.getQuantidade() + quantidade);
			} else {
				items.add(new Item(produto, valorUnitario, quantidade));
			}
		} catch (Exception e) {
			throw new AdicionarItemException(e);
		}
	}

	/**
	 * Permite a remoção do item que representa este produto do carrinho de compras.
	 *
	 * @param produto
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 *         false caso o produto não exista no carrinho.
	 */
	public boolean removerItem(Produto produto) {
		return items.removeIf(item -> item.getProduto().equals(produto));
	}

	/**
	 * Permite a remoção do item de acordo com a posição. Essa posição deve ser determinada pela
	 * ordem de inclusão do produto na coleção, em que zero representa o primeiro item.
	 *
	 * @param posicaoItem
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 *         false caso o produto não exista no carrinho.
	 */
	public boolean removerItem(int posicaoItem) {
		try {
			items.remove(posicaoItem);
			return true;
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
	}

	/**
	 * Retorna o valor total do carrinho de compras, que deve ser a soma dos valores totais de todos
	 * os itens que compõem o carrinho.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTotal() {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		for (Item item : items) {
			valorTotal = valorTotal.add(item.getValorTotal());
		}
		return valorTotal;

	}

	/**
	 * Retorna a lista de itens do carrinho de compras.
	 *
	 * @return itens
	 */
	public Collection<Item> getItens() {
		return items;

	}
	
	public Optional<Item> getItem(Long codigo){
		return 	items.stream().filter(item -> item.getProduto().getCodigo().equals(codigo)).findFirst();
	}
}