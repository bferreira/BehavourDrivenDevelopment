$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("LogParser.feature");
formatter.feature({
  "line": 1,
  "name": "Log Parser",
  "description": "\r\nThe goal is to write a parser in Java that parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. \r\nCreate a java tool that can parse and load the given log file to MySQL. The delimiter of the log file is pipe (|)\r\nThe tool takes \"startDate\", \"duration\" and \"threshold\" as command line arguments. \"startDate\" is of \"yyyy-MM-dd.HH:mm:ss\" format, \"duration\" can take only \"hourly\", \"daily\" as inputs and \"threshold\" can be an integer.\r\nThis is how the tool works:\r\n  java -cp \"parser.jar\" com.ef.Parser --startDate\u003d2017-01-01.13:00:00 --duration\u003dhourly --threshold\u003d100\r\nThe tool will find any IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00 (one hour) and print them to console AND also load them to another MySQL table with comments on why it\u0027s blocked.\r\njava -cp \"parser.jar\" com.ef.Parser --startDate\u003d2017-01-01.13:00:00 --duration\u003ddaily --threshold\u003d250\r\nThe tool will find any IPs that made more than 250 requests starting from 2017-01-01.13:00:00 to 2017-01-02.13:00:00 (24 hours) and print them to console AND also load them to another MySQL table with comments on why it\u0027s blocked.",
  "id": "log-parser",
  "keyword": "Feature"
});
formatter.before({
  "duration": 501747037,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Launch LogParserApp with duration hourly",
  "description": "",
  "id": "log-parser;launch-logparserapp-with-duration-hourly",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "User Launch LogParserApp with commandline java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/accessHourly.log --startDate\u003d2017-01-01.15:00:00 --duration\u003dhourly --threshold\u003d1",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "Assert ipblacklist table has",
  "rows": [
    {
      "cells": [
        "192.168.102.136"
      ],
      "line": 16
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "Assert log table has all lines from the txt file",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/accessHourly.log --startDate\u003d2017-01-01.15:00:00 --duration\u003dhourly --threshold\u003d1",
      "offset": 42
    }
  ],
  "location": "ParserSteps.user_Launch_LogParserApp_with_commandline_parser_jar(String)"
});
formatter.result({
  "duration": 167224888,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_ipblacklist_has(String\u003e)"
});
formatter.result({
  "duration": 3097922,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_log_table_has_all_lines_from_the_txt_file()"
});
formatter.result({
  "duration": 1357231,
  "status": "passed"
});
formatter.after({
  "duration": 415595,
  "status": "passed"
});
formatter.before({
  "duration": 211853928,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Launch LogParserApp with duration daily",
  "description": "",
  "id": "log-parser;launch-logparserapp-with-duration-daily",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "User Launch LogParserApp with commandline java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/accessDaily.log --startDate\u003d2017-01-01.00:00:00 --duration\u003ddaily --threshold\u003d0",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "Assert ipblacklist table has",
  "rows": [
    {
      "cells": [
        "192.168.102.136"
      ],
      "line": 22
    },
    {
      "cells": [
        "192.168.102.137"
      ],
      "line": 23
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Assert log table has all lines from the txt file",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/accessDaily.log --startDate\u003d2017-01-01.00:00:00 --duration\u003ddaily --threshold\u003d0",
      "offset": 42
    }
  ],
  "location": "ParserSteps.user_Launch_LogParserApp_with_commandline_parser_jar(String)"
});
formatter.result({
  "duration": 84979533,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_ipblacklist_has(String\u003e)"
});
formatter.result({
  "duration": 555747,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_log_table_has_all_lines_from_the_txt_file()"
});
formatter.result({
  "duration": 1180894,
  "status": "passed"
});
formatter.after({
  "duration": 426937,
  "status": "passed"
});
formatter.before({
  "duration": 210028983,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Launch LogParserApp and ignore log entries before start time",
  "description": "",
  "id": "log-parser;launch-logparserapp-and-ignore-log-entries-before-start-time",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "User Launch LogParserApp with commandline java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/ignoreLogsBeforeStartTime.log --startDate\u003d2017-01-01.01:00:00 --duration\u003ddaily --threshold\u003d1",
  "keyword": "Given "
});
formatter.step({
  "line": 28,
  "name": "Assert ipblacklist table has",
  "rows": [
    {
      "cells": [
        "192.168.234.82"
      ],
      "line": 29
    },
    {
      "cells": [
        "192.168.234.83"
      ],
      "line": 30
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "Assert log table has all lines from the txt file",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/ignoreLogsBeforeStartTime.log --startDate\u003d2017-01-01.01:00:00 --duration\u003ddaily --threshold\u003d1",
      "offset": 42
    }
  ],
  "location": "ParserSteps.user_Launch_LogParserApp_with_commandline_parser_jar(String)"
});
formatter.result({
  "duration": 31423983,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_ipblacklist_has(String\u003e)"
});
formatter.result({
  "duration": 591392,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_log_table_has_all_lines_from_the_txt_file()"
});
formatter.result({
  "duration": 1181164,
  "status": "passed"
});
formatter.after({
  "duration": 556017,
  "status": "passed"
});
formatter.before({
  "duration": 232615300,
  "status": "passed"
});
formatter.scenario({
  "line": 32,
  "name": "Launch LogParserApp with hourly duration and the big log(acess.log)",
  "description": "",
  "id": "log-parser;launch-logparserapp-with-hourly-duration-and-the-big-log(acess.log)",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "User Launch LogParserApp with commandline java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/access.log --startDate\u003d2017-01-01.15:00:00 --duration\u003dhourly --threshold\u003d200",
  "keyword": "Given "
});
formatter.step({
  "line": 35,
  "name": "Assert ipblacklist table has",
  "rows": [
    {
      "cells": [
        "192.168.11.231"
      ],
      "line": 36
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/access.log --startDate\u003d2017-01-01.15:00:00 --duration\u003dhourly --threshold\u003d200",
      "offset": 42
    }
  ],
  "location": "ParserSteps.user_Launch_LogParserApp_with_commandline_parser_jar(String)"
});
formatter.result({
  "duration": 3440431020,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_ipblacklist_has(String\u003e)"
});
formatter.result({
  "duration": 784742,
  "status": "passed"
});
formatter.after({
  "duration": 340253,
  "status": "passed"
});
formatter.before({
  "duration": 207222166,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "Launch LogParserApp with daily duration and the big log(acess.log)",
  "description": "",
  "id": "log-parser;launch-logparserapp-with-daily-duration-and-the-big-log(acess.log)",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 40,
  "name": "User Launch LogParserApp with commandline java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/access.log --startDate\u003d2017-01-01.00:00:00 --duration\u003ddaily --threshold\u003d500",
  "keyword": "Given "
});
formatter.step({
  "line": 41,
  "name": "Assert ipblacklist table has",
  "rows": [
    {
      "cells": [
        "192.168.129.191"
      ],
      "line": 42
    },
    {
      "cells": [
        "192.168.38.77"
      ],
      "line": 43
    },
    {
      "cells": [
        "192.168.143.177"
      ],
      "line": 44
    },
    {
      "cells": [
        "192.168.162.248"
      ],
      "line": 45
    },
    {
      "cells": [
        "192.168.199.209"
      ],
      "line": 46
    },
    {
      "cells": [
        "192.168.51.205"
      ],
      "line": 47
    },
    {
      "cells": [
        "192.168.31.26"
      ],
      "line": 48
    },
    {
      "cells": [
        "192.168.203.111"
      ],
      "line": 49
    },
    {
      "cells": [
        "192.168.33.16"
      ],
      "line": 50
    },
    {
      "cells": [
        "192.168.62.176"
      ],
      "line": 51
    },
    {
      "cells": [
        "192.168.219.10"
      ],
      "line": 52
    },
    {
      "cells": [
        "192.168.206.141"
      ],
      "line": 53
    },
    {
      "cells": [
        "192.168.52.153"
      ],
      "line": 54
    },
    {
      "cells": [
        "192.168.185.164"
      ],
      "line": 55
    },
    {
      "cells": [
        "192.168.102.136"
      ],
      "line": 56
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "java -cp \"parser.jar\" com.ef.Parser --accesslog\u003dsrc/test/resources/access.log --startDate\u003d2017-01-01.00:00:00 --duration\u003ddaily --threshold\u003d500",
      "offset": 42
    }
  ],
  "location": "ParserSteps.user_Launch_LogParserApp_with_commandline_parser_jar(String)"
});
formatter.result({
  "duration": 2956058758,
  "status": "passed"
});
formatter.match({
  "location": "ParserSteps.assert_ipblacklist_has(String\u003e)"
});
formatter.result({
  "duration": 686446,
  "status": "passed"
});
formatter.after({
  "duration": 354835,
  "status": "passed"
});
});