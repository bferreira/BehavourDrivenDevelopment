Feature: Log Parser 

	The goal is to write a parser in Java that parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. 
Create a java tool that can parse and load the given log file to MySQL. The delimiter of the log file is pipe (|)
The tool takes "startDate", "duration" and "threshold" as command line arguments. "startDate" is of "yyyy-MM-dd.HH:mm:ss" format, "duration" can take only "hourly", "daily" as inputs and "threshold" can be an integer.
This is how the tool works:
    java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
	The tool will find any IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00 (one hour) and print them to console AND also load them to another MySQL table with comments on why it's blocked.
	java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250
	The tool will find any IPs that made more than 250 requests starting from 2017-01-01.13:00:00 to 2017-01-02.13:00:00 (24 hours) and print them to console AND also load them to another MySQL table with comments on why it's blocked.

Scenario: Launch LogParserApp with duration hourly 

	Given User Launch LogParserApp with commandline java -cp "parser.jar" com.ef.Parser --accesslog=src/test/resources/accessHourly.log --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=1 
	Then Assert ipblacklist table has 
		|192.168.102.136|
	Then Assert log table has all lines from the txt file 
Scenario: Launch LogParserApp with duration daily 

	Given User Launch LogParserApp with commandline java -cp "parser.jar" com.ef.Parser --accesslog=src/test/resources/accessDaily.log --startDate=2017-01-01.00:00:00 --duration=daily --threshold=0 
	Then Assert ipblacklist table has 
		|192.168.102.136|
		|192.168.102.137| 
	Then Assert log table has all lines from the txt file 
Scenario: Launch LogParserApp and ignore log entries before start time 

	Given User Launch LogParserApp with commandline java -cp "parser.jar" com.ef.Parser --accesslog=src/test/resources/ignoreLogsBeforeStartTime.log --startDate=2017-01-01.01:00:00 --duration=daily --threshold=1 
	Then Assert ipblacklist table has 
		|192.168.234.82| 
		|192.168.234.83|
	Then Assert log table has all lines from the txt file 
Scenario: Launch LogParserApp with hourly duration and the big log(acess.log) 

	Given User Launch LogParserApp with commandline java -cp "parser.jar" com.ef.Parser --accesslog=src/test/resources/access.log --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200 
	Then Assert ipblacklist table has 
		|192.168.11.231|
		
Scenario: Launch LogParserApp with daily duration and the big log(acess.log) 

	Given User Launch LogParserApp with commandline java -cp "parser.jar" com.ef.Parser --accesslog=src/test/resources/access.log --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500 
	Then Assert ipblacklist table has 
		|192.168.129.191|
		|192.168.38.77|
		|192.168.143.177|
		|192.168.162.248|
		|192.168.199.209|
		|192.168.51.205|
		|192.168.31.26|
		|192.168.203.111|
		|192.168.33.16|
		|192.168.62.176|
		|192.168.219.10|
		|192.168.206.141|
		|192.168.52.153|
		|192.168.185.164|
		|192.168.102.136|