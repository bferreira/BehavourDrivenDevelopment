package bdd.acceptance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ef.Parser;
import com.ef.business.LogLine;
import com.ef.business.commandline.CommandLineOptions;
import com.ef.business.reader.LogReader;
import com.ef.business.writer.LogDBWriter;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
public class ParserSteps {
	private static Connection connection;
	private String[] commandline;

	@Before
	public static void setUp() throws SQLException, IOException {
		connection = LogDBWriter.createDBConnection();
		Statement st = connection.createStatement();
		st.addBatch("truncate table ipblacklist");
		st.addBatch("truncate table log");
		st.executeBatch();
		connection.commit();
	}
	
	@After
	public static void close() throws SQLException, IOException {
		connection.close();
	}
	
	@Given("^User Launch LogParserApp with commandline (.*)$")
	public void user_Launch_LogParserApp_with_commandline_parser_jar(String commandline) throws Throwable {
		this.commandline = commandline.split(" ");
		new Parser(this.commandline).launch();
	}

	@Then("^Assert ipblacklist table is empty\\.$")
	public void Assert_ipblacklist_table_is_empty() throws Throwable {
		ResultSet rs = connection
				.createStatement()
				.executeQuery("select count(*) from ipblacklist");
		rs.next();
		assertEquals(0, rs.getInt("count(*)"));
	}
	
	@Then("^Assert ipblacklist table has$")
	public void assert_ipblacklist_has(List<String> ips) throws Throwable {
		ResultSet rs = connection
				.createStatement()
				.executeQuery("select ip from ipblacklist as ibl where ibl.ip in (\""+String.join("\",\"", ips)+ "\")");
		List<String> ipsFromTable = new ArrayList<>();
		while(rs.next()) ipsFromTable.add(rs.getString("ip"));
		List<String> ipsNotFound = new ArrayList<>(ips);
		ipsNotFound.removeAll(ipsFromTable);
		
		assertTrue("the follow ips weren't found at ipblacklist table: " + String.join(", ", ipsNotFound),ipsNotFound.isEmpty());
	}
	
	@Then("^Assert log table has all lines from the txt file$")
	public void assert_log_table_has_all_lines_from_the_txt_file() throws Throwable {
		ResultSet rs = connection
				.createStatement()
				.executeQuery("select * from log");
		List<LogLine> logLineFromDataBase = new ArrayList<>();
		List<LogLine> logLineFromLogFile = LogReader.getFileReader(new CommandLineOptions(commandline)).loadInMemoryLogLines();
		while(rs.next()) {
			logLineFromDataBase.add(new LogLine(
					rs.getString("time"),
					rs.getString("ip"),
					rs.getString("method"),
					rs.getString("protocol"),
					rs.getString("status"),
					rs.getString("browser")));
		}
		assertTrue(logLineFromDataBase.containsAll(logLineFromLogFile));
	}
}
