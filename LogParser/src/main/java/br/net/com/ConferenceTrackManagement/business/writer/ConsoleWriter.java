package br.net.com.ConferenceTrackManagement.business.writer;

import br.net.com.ConferenceTrackManagement.business.conference.ConferenceSchedule;

public class ConsoleWriter implements Writer {

	@Override
	public void write(ConferenceSchedule consolidedConferenceSchedule) {
        System.out.println(consolidedConferenceSchedule);
	}
}
