package br.net.com.ConferenceTrackManagement.business.reader;

import java.util.List;

import br.net.com.ConferenceTrackManagement.business.conference.Activity;

public interface Reader {
	public abstract List<Activity> read() throws Exception;
}
