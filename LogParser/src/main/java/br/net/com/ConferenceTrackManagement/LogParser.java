package br.net.com.ConferenceTrackManagement;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class LogParser {
	static DateTimeFormatter startTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
	static DateTimeFormatter logTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	static LocalDateTime startDatime = LocalDateTime.parse("2017-01-01.13:00:00", startTimeFormatter);
	static LocalDateTime rollingTreshold = startDatime;
	static Map<String, Long> ipCount = new HashMap<>();
	static String duration = "daily";
	static Long ipTreshold = 1L;
	static List<String> ipAboveTreshold = new ArrayList<>();

	public static void main(String[] args) {
		try (Stream<String> stream = Files.lines(Paths.get(
				"C:\\Users\\bruno.ferreira.silva\\git\\BehavourDrivenDevelopment\\LogParser\\src\\test\\resources\\access.log"))) {
			rollOutDuration();
			stream.forEach(line -> {
				String[] lineSplitted = line.split("\\|");
				String ip = lineSplitted[1];
				String logTime = lineSplitted[0];
				if (ipAboveTreshold.contains(ip) == false) {// IGNORE IPS ALLREADY BOOKED
					if (isLogTimeBetweenStartAndTreshold(logTime)) {
						countIp(ip);
					} else {
						rollOutDuration();
					}
				}
			});
		} catch (IOException e) {
			System.out.println(e);
		}
		ipAboveTreshold.stream().forEach(System.out::println);
	}

	private static boolean isLogTimeBetweenStartAndTreshold(String logTimeString) {
		LocalDateTime logTime = LocalDateTime.parse(logTimeString, logTimeFormatter);
		return (logTime.isAfter(startDatime) || logTime.isEqual(startDatime))
			&& (logTime.isBefore(rollingTreshold) || logTime.isEqual(rollingTreshold));
	}

	private static void countIp(String ip) {
		Long quantity = ipCount.getOrDefault(ip, 0L);
		if (++quantity > ipTreshold)
			ipAboveTreshold.add(ip);
		else
			ipCount.put(ip, quantity);
	}

	private static void rollOutDuration() {
		switch (duration) {
		case "hourly":
			rollingTreshold = rollingTreshold.plusHours(1);
			break;
		case "daily":
			rollingTreshold = rollingTreshold.plusDays(1);
			break;
		}
		ipCount.clear();
	}
}
