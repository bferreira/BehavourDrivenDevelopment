package br.net.com.ConferenceTrackManagement.commandline.app;

import java.util.regex.*;

class Main {
	public static void main(String[] args) {
		String txt = "|192.168.14.124|";
		String re1 = "(\\|)"; // Pipe Character 1
		String re2 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";// IP Address
		String re3 = "(\\|)"; // Pipe Character 2
		Pattern p = Pattern.compile(re1 + re2 + re3, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher m = p.matcher(txt);
		if (m.find()) {
			String c1 = m.group(1);
			String ipaddress1 = m.group(2);
			String c2 = m.group(3);
			System.out.print(
					"(" + c1.toString() + ")" + "(" + ipaddress1.toString() + ")" + "(" + c2.toString() + ")" + "\n");
		}
	}
}
