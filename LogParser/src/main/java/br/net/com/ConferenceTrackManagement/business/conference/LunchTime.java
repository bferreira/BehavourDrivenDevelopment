package br.net.com.ConferenceTrackManagement.business.conference;

import java.time.LocalTime;

public class LunchTime extends Activity {
	private static final String LUNCH = "Lunch";

	public LunchTime() {
		super(LUNCH, LocalTime.of(12, 00));
	}

}