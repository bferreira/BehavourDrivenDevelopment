package br.net.com.ConferenceTrackManagement.business.reader;

import br.net.com.ConferenceTrackManagement.business.commandline.OptionsParser;
import br.net.com.ConferenceTrackManagement.business.reader.file.FileReader;
import br.net.com.ConferenceTrackManagement.exceptions.ExceptionMessages;

/**
 * Create the instance for ConferenceTrackReader
 * @author bruno.ferreira.silva
 *
 */
public class ReaderFactory {
	
	/**
	 * Get ConferenceTrackReader from command line options
	 * @param options
	 * @return
	 */
	public Reader get(OptionsParser options) {
		String readerType = options.get("inputType",ExceptionMessages.NO_INPUT_TYPE);
		switch (readerType) {
			case "file":
				String filePath = options.get("path",ExceptionMessages.INPUT_FILE_PATH_EMPTY);
				return new FileReader(filePath);
			default:
				throw new IllegalArgumentException(ExceptionMessages.INVALID_INPUT_TYPE);
		}
	}
}
