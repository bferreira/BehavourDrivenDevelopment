package com.ef;

import com.ef.business.commandline.CommandLineOptions;
import com.ef.business.controller.LogParserController;

public class Parser {
	private CommandLineOptions options;

	public Parser(String[] args) throws Exception {
		options = new CommandLineOptions(args);
	}

	public void launch() throws Exception {
		new LogParserController(options).execute();
	}

	public static void main(String[] args) throws Exception {
		new Parser(args).launch();
	}

}
