package com.ef.business.commandline;
import java.util.HashMap;
import java.util.Map;

import com.ef.exceptions.InvalidCommandLineArgumentException;
/**
 * Parse command line arguments divided by the occurrence of "=" <br>
 * For instance, this command line: <br> 
 * java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250 <br> 
 * Will result in this map:<br> 
 * {"--startDate", "2017-01-01.13:00:00"} <br>
 * {"--duration", "daily"} <br>
 * {"--threshold", "250"} <br>
 * @param commandline args
 * @author bruno.ferreira.silva
 */
public class CommandLineOptions {
	Map<String, String> commandLineArguments;
	
	public CommandLineOptions(String[] args) {
		this.commandLineArguments = parseArgsToHashMap(args);
//		System.out.println("Running with command line arguments");
//		commandLineArguments.entrySet().stream().forEach(System.out::println);
	}
	
	public String get(String key) {
		return get(key, null);
	}
	
	public String getRequired(String key) {
		return get(key, "Command line argument missing: " + key);
	}
	
	public String get(String key, String notFoundExceptionMessage) {
		String value = commandLineArguments.get(key);
		if (notFoundExceptionMessage != null && value == null) {
			throw new InvalidCommandLineArgumentException(notFoundExceptionMessage);
		}
		return value;

	}
	
	private Map<String, String> parseArgsToHashMap(String[] args) {
		Map<String, String> argsMap = new HashMap<>();
		for (String arg : args) {
			String[] keyValue = arg.split("=", 2);
			if(keyValue.length>1) // ignore when argument don't match '=' token 
			argsMap.put(keyValue[0], keyValue[1]);
		}
		return argsMap;
	}
}
