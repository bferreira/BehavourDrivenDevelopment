package com.ef.business.writer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

import com.ef.Parser;
import com.ef.business.BlockedIp;
import com.ef.business.LogLine;

public class LogDBWriter implements LogWriter {
	private static final String UPDATE_IP_QUERY = "update ipblacklist set reason=? where ip=?";
	private static final String INSERRT_IP_QUERY = "insert into ipblacklist(ip,reason) values(?,?)";
	private static final String INSERRT_LOG_LINE = "insert into log(time,ip,method,protocol,status,browser) values(?,?,?,?,?,?)";
	
	@Override
	public void insertLogLines(List<LogLine> lines) throws Exception {
		Connection connection = createDBConnection();
		try {
			PreparedStatement ps = connection.prepareStatement(INSERRT_LOG_LINE);
			for (LogLine logLine : lines) {
				ps.setString(1, logLine.getLogTime());
				ps.setString(2, logLine.getIp());
				ps.setString(3, logLine.getMethod());
				ps.setString(4, logLine.getProtocol());
				ps.setInt(5, Integer.valueOf(logLine.getStatus()));
				ps.setString(6, logLine.getBrowser());
				ps.addBatch();
			}
			ps.executeBatch();
			connection.commit();
		}finally {
			connection.close();
		}
	}
	
	@Override
	public void insertIpToBlackList(List<BlockedIp> ips) throws Exception {
		Connection connection = createDBConnection();
		try {
			for (BlockedIp blockedIp : ips) {
				System.out.println(blockedIp.getIp());
				PreparedStatement ps = connection.prepareStatement(INSERRT_IP_QUERY);
				ps.setString(1, blockedIp.getIp());
				ps.setString(2, blockedIp.getReason());
				try {
					ps.execute();
				}catch (SQLIntegrityConstraintViolationException e) {
					ps = connection.prepareStatement(UPDATE_IP_QUERY);
					ps.setString(1, blockedIp.getReason());
					ps.setString(2, blockedIp.getIp());
					ps.execute();
				} 
			}
			connection.commit();
		}finally {
			connection.close();
		}
	}
	
	public static Connection createDBConnection() throws SQLException, IOException {
		Properties prop = new Properties();
		prop.load(Parser.class.getClassLoader().getResourceAsStream("config.properties"));
		BasicDataSource ds = new BasicDataSource();
	    ds.setDriverClassName(prop.getProperty("driver"));
	    ds.setUrl(prop.getProperty("database"));
	    ds.setUsername(prop.getProperty("user"));
	    ds.setPassword(prop.getProperty("password"));
	    ds.setValidationQuery("select 1");
	    ds.setDefaultAutoCommit(false);
		return ds.getConnection();
	}
}
