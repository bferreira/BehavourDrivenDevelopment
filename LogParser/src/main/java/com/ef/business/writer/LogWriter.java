package com.ef.business.writer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ef.business.BlockedIp;
import com.ef.business.LogLine;

public interface LogWriter {
	public void insertLogLines(List<LogLine> lines) throws Exception;
	public void insertIpToBlackList(List<BlockedIp> logLinesAboveTreshold) throws Exception;
	
	public static LogWriter getLogWriter() throws SQLException, IOException {
		return new LogDBWriter();
	}
}
