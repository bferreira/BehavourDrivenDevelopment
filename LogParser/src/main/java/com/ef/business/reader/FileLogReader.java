package com.ef.business.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.ef.business.LogLine;

/**
 * Class responsible for read log text file and pass it to memory
 * 
 * @author Bruno.Ferreira.Silva
 *
 */
public class FileLogReader implements LogReader {
	private String filePath;

	public FileLogReader(String filePath) {
		this.filePath = filePath;
	}

	public List<LogLine> loadInMemoryLogLines() throws IOException {
		return Files.lines(Paths.get(filePath)).map(line -> {
			String[] lineSplitted = line.split("\\|");
			String logTime = lineSplitted[0];
			String ip = lineSplitted[1];
			String[] request = lineSplitted[2].replace("\"", "").split("/",2);
			String method = request[0];
			String protocol = request[1];
			String status = lineSplitted[3];
			String browser = lineSplitted[4];
			return new LogLine(logTime, ip, method, protocol, status, browser);
		}).collect(Collectors.toList());
	}

}
