package com.ef.business.reader;

import java.util.List;

import com.ef.business.LogLine;
import com.ef.business.commandline.CommandLineOptions;

/**
 * Interface responsible to parse log.
 * @author bruno.ferreira.silva
 */
public interface LogReader {
	public List<LogLine> loadInMemoryLogLines() throws Exception;

	/**
	 * Returns implementation of a file log parser base on the command line
	 * @param options
	 * @return
	 */
	public static LogReader getFileReader(CommandLineOptions options) {
		return new FileLogReader(options.getRequired("--accesslog"));
	}
}
