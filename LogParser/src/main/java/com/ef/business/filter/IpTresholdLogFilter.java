package com.ef.business.filter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ef.business.BlockedIp;
import com.ef.business.LogLine;

/**
 * Class responsible for filter IP's that made more than a certain number of requests for the given duration. 
 */
public class IpTresholdLogFilter implements LogFilter {
	private DateTimeFormatter startTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
	private DateTimeFormatter logTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	private LocalDateTime rollingTreshold;
	private Map<String, Long> ipCount = new HashMap<>();
	private List<BlockedIp> ipBlackList = new ArrayList<>();
	private String reasonToBlock;

	private String duration;
	private Long ipTreshold;
	private LocalDateTime startDatime;

	public IpTresholdLogFilter(String duration, String ipTreshold, String startDatime) {
		this.duration = duration;
		this.ipTreshold = Long.valueOf(ipTreshold);
		this.startDatime = LocalDateTime.parse(startDatime, startTimeFormatter);
		this.rollingTreshold = this.startDatime;
		this.reasonToBlock = new StringBuilder()
							.append("this ip is blocked because made more then ")
							.append(ipTreshold)
							.append(" requests allowed ")
							.append(duration)
							.toString();
//		System.out.println("FileLogParser created");
	}

	@Override
	public List<BlockedIp> getLogLinesAboveTreshold(List<LogLine> lines) throws IOException {
			rollOutDuration();
			for (LogLine line : lines) {
				if (isLogTimeBetweenStartAndDurationTreshold(line.getLogTime())) {
					countip(line.getIp());
				} else {
					rollOutDuration();
				}
			}
//		System.out.println("FileLogParser terminated with a total of ips: "+ ipBlackList.size());
		return ipBlackList;
	}
	
	private void rollOutDuration() {
		switch (duration) {
			case "hourly":
				rollingTreshold = rollingTreshold.plusHours(1);
				break;
			case "daily":
				rollingTreshold = rollingTreshold.plusDays(1);
				break;
		}
		ipCount.clear();
	}

	private boolean isLogTimeBetweenStartAndDurationTreshold(String logTimeString) {
		LocalDateTime logTime = LocalDateTime.parse(logTimeString, logTimeFormatter);
		return (logTime.isAfter(startDatime) || logTime.isEqual(startDatime))
				&& (logTime.isBefore(rollingTreshold) || logTime.isEqual(rollingTreshold));
	}

	private void countip(String ip) {
		Long quantity = ipCount.getOrDefault(ip, 0L);
		if (++quantity > ipTreshold) {
			BlockedIp e = new BlockedIp(ip,reasonToBlock);
			if(ipBlackList.contains(e) == false) ipBlackList.add(e);
		} else
			ipCount.put(ip, quantity);
	}

	@Override
	public String toString() {
		return "FileLogParser [duration=" + duration + ", ipTreshold=" + ipTreshold
				+ ", startDatime=" + startDatime + "]";
	}
}
