package com.ef.business.filter;

import java.io.IOException;
import java.util.List;

import com.ef.business.BlockedIp;
import com.ef.business.LogLine;
import com.ef.business.commandline.CommandLineOptions;

public interface LogFilter {
	List<BlockedIp> getLogLinesAboveTreshold(List<LogLine> lines) throws IOException;

	static LogFilter getLogParser(CommandLineOptions options) {
		return new IpTresholdLogFilter(
				options.getRequired("--duration"), 
				options.getRequired("--threshold"),
				options.getRequired("--startDate"));
	}
}
