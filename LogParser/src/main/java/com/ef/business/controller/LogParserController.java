package com.ef.business.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ef.business.LogLine;
import com.ef.business.commandline.CommandLineOptions;
import com.ef.business.filter.LogFilter;
import com.ef.business.reader.LogReader;
import com.ef.business.writer.LogWriter;

/**
 * Class responsible to read and write the log.
 * @author Bruno Ferreira da Silva
 *
 */
public class LogParserController {
	private LogReader reader;
	private LogFilter filter;
	private LogWriter writer;
	
	public LogParserController(CommandLineOptions options) throws SQLException, IOException {
		reader = LogReader.getFileReader(options);
		filter = LogFilter.getLogParser(options);
		writer = LogWriter.getLogWriter();
	}

	public void execute() throws Exception {
			List<LogLine> logLInes = reader.loadInMemoryLogLines();
			writer.insertLogLines(logLInes);
			writer.insertIpToBlackList(filter.getLogLinesAboveTreshold(logLInes));
	}
}
